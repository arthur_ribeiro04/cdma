function original_sequence = recover_sequence(received_signal, SS, L, Nb)
  
  sKey = length(SS);
  aux = L/(Nb*sKey);
  ss_Size  = Nb*sKey;
  spreading_sequence = zeros(1,ss_Size);
  j=1;
  
  for i=1 : ss_Size 
    spreading_sequence(i) = sum(received_signal(1+(i-1)*aux:aux*i))/aux;
  end
  aux = sKey;
  original_sequence = zeros(1,Nb);
  for i=1 : Nb 
    original_sequence(i) = sum(spreading_sequence(1+(i-1)*aux:aux*i))/aux;
  end
  
  for i=1:Nb
    if original_sequence (i) > 0
        original_sequence (i) = 1;
    else
        original_sequence (i) = 0;
    end
  end
  
  end


