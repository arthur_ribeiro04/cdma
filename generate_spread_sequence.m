function spread_sequence = generate_spread_sequence( orig_sequence ,SS)

sKey = length(SS);
Nb = length(orig_sequence);
spread_sequence = zeros(1,length(SS)*Nb);


for i=1:Nb
    if orig_sequence(1,i) == 0
        orig_sequence(1,i) = -1;
    end
end

for i=1 : Nb
    for j=1 : sKey
    spread_sequence((i-1)*sKey+j) = SS(j)* orig_sequence(i);
    end
end

end

