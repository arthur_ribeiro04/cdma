clear;
close all;

% Sequ�ncias de espalhamento
SS1 = [1 -1 -1 1 -1 1]; %usu�rio 1
SS2 = [1 1 -1 -1 1 1]; %usu�rio 2

% Mensagens
original_sequence_1 = [1 1 0 1 1 0 1 0]; % sequ�ncia original do usu�rio 1
original_sequence_2 = [0 1 1 1 1 0 1 1]; % sequ�ncia original do usu�rio 2

% Constantes
Fs = 480;                       %frequ�ncia de amostragem
Tb = 1;                         %tempo de um bit enviado
Nb = 8;                         %n�mero de bits
Fc = 10;                        %frequ�ncia da portadora
Ts = 1/Fs;                      %per�odo de amostragem
t = 0:Ts:(Nb*Tb-Ts);            %vetor do tempo
L = length(t);                  %comprimento do vetor tempo
NFFT = 2^nextpow2(L);           %n�mero de pontos a serem utilizados na FFT
f = Fs/2*linspace(-1,1,NFFT);   %vetor frequ�ncia (para sinais no dom�nio frequ�ncia)

% 1)
original_signal_1 = generate_signal_from_binary_sequence(original_sequence_1, L);
original_signal_2 = generate_signal_from_binary_sequence(original_sequence_2, L);

% Plot dos sinais correspondentes as mensagens
sent_message_1 = strcat('Sequ�ncia de bits originais do usu�rio 1: ', mat2str(original_sequence_1));
plot_signal_in_time(sent_message_1, original_signal_1, t, Nb, 'Mensagem enviada 1');

sent_message_2 = strcat('Sequ�ncia de bits originais do usu�rio 2: ', mat2str(original_sequence_2));
plot_signal_in_time(sent_message_2, original_signal_2, t, Nb, 'Mensagem enviada 2');

% 2) 
spread_sequence_signal_1 = generate_spread_sequence_signal(original_sequence_1, SS1, L);
spread_sequence_signal_2 = generate_spread_sequence_signal(original_sequence_2, SS2, L);

% Plot dos sinais correspondentes aos bits espalhados

spread_message_1 = strcat('Sequ�ncia de bits espalhada do usu�rio 1: ', mat2str(SS1));
plot_signal_in_time(spread_message_1, spread_sequence_signal_1, t, Nb, 'Sequ�ncia espalhada 1');

spread_message_2 = strcat('Sequ�ncia de bits espalhada do usu�rio 2: ', mat2str(SS2));
plot_signal_in_time(spread_message_2, spread_sequence_signal_2, t, Nb, 'Sequ�ncia espalhada 2');

% 3) Aplica a transformada de Fourier, movendo as componentes de frequ�ncia
% zero para o centro do array, e realiza o plot do sinal resultante no dom�nio 
% frequ�ncia.

frequency_original_signal_1 = fftshift(fft(original_signal_1, NFFT));
freq_sent_message_1 = strcat('Sinal com os bits originais do usu�rio 1: ', mat2str(original_sequence_1));
plot_signal_in_frequency(freq_sent_message_1, frequency_original_signal_1, f, 'Mensagem enviada 1')

frequency_original_signal_2 = fftshift(fft(original_signal_2, NFFT));
freq_sent_message_2 = strcat('Sinal com os bits originais do usu�rio 2: ', mat2str(original_sequence_2));
plot_signal_in_frequency(freq_sent_message_2, frequency_original_signal_2, f, 'Mensagem enviada 2')

frequency_spread_signal_1 = fftshift(fft(spread_sequence_signal_1, NFFT));
freq_spread_message_1 = strcat('Sinal com os bits espalhados do usu�rio 1: ', mat2str(SS1));
plot_signal_in_frequency(freq_spread_message_1, frequency_spread_signal_1, f, 'Sinal espalhado 1')

frequency_spread_signal_2 = fftshift(fft(spread_sequence_signal_2, NFFT));
freq_spread_message_2 = strcat('Sinal com os bits espalhados do usu�rio 2: ', mat2str(SS2));
plot_signal_in_frequency(freq_spread_message_2, frequency_spread_signal_2, f, 'Sinal espalhado 2')

% 4) Transmite os dois sinais espalhados simultaneamente (opera��o de soma)
send_signal = spread_sequence_signal_1 + spread_sequence_signal_2;

% Transforma o sinal resultante para o dom�nio frequ�ncia
send_signal_in_frequency = fftshift(fft(send_signal, NFFT));
filtered_signal = send_signal_in_frequency;

% Filtra o sinal no dom�nio frequ�ncia
filterOffset = NFFT/Fc; % < -409.6 ou > 409.6

for frequency = 1:length(f)
    if f(frequency) < -filterOffset || f(frequency) > filterOffset
        filtered_signal(frequency) = 0;
    end
end

% Aplica a transformada inversa de Fourier para recuperar o sinal no
% dom�nio tempo.
received_signal = real(ifft(ifftshift(filtered_signal)));
received_signal = received_signal(1:L);

% Gera o sinais correspondentes �s sequ�ncias de espalhamento dos usu�rios
spreading_signal_1 = generate_signal_from_spreading_sequence(SS1, L, Nb);
spreading_signal_2 = generate_signal_from_spreading_sequence(SS2, L, Nb);

% Multiplica o sinal recebido pelo sinal gerado a partir da sequ�ncia de
% espalhamento de cada usu�rio
received_signal_user_1 = received_signal .* spreading_signal_1;
received_signal_user_2 = received_signal .* spreading_signal_2;

% Recupera a sequ�ncia enviada a partir do sinal resultante
received_sequence_user_1 = recover_received_sequence(received_signal_user_1, SS1, L, Nb);
received_sequence_user_2 = recover_received_sequence(received_signal_user_2, SS2, L, Nb);

% Plot das sequ�ncias recuperadas como sinais no dom�nio tempo
received_signal_user_1 = generate_signal_from_binary_sequence(received_sequence_user_1, L);
received_message_1 = strcat('Sequ�ncia de bits recuperada do usu�rio 1: ', mat2str(received_sequence_user_1));
plot_signal_in_time(received_message_1, received_signal_user_1, t, Nb, 'Mensagem recebida 1');

received_signal_user_2 = generate_signal_from_binary_sequence(received_sequence_user_2, L);
received_message_2 = strcat('Sequ�ncia de bits recuperada do usu�rio 2: ', mat2str(received_sequence_user_2));
plot_signal_in_time(received_message_2, received_signal_user_2, t, Nb, 'Mensagem recebida 2');