function spread_signal = generate_spread_signal(spread_sequence, L)

seq_size = length(spread_sequence(1,:));
spread_signal = zeros(1,L);
size_aux = L/seq_size;
for i=1 : seq_size
    for j=1 : size_aux
     spread_signal((i-1)*size_aux+j) = spread_sequence(1,i); 
    end
end

end

