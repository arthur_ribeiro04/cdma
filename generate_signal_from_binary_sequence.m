function signal = generate_signal_from_binary_sequence(sequence, L)

    % Para cada bit da sequ�ncia bin�ria (0's ou 1's) fornecida, popula 
    % o vetor signal com os valores da sequ�ncia (-1's ou 1's) ao longo 
    % do tempo (de comprimento L).

    signal = zeros(1, L);
    signal_sample_index = 1;
    
    for bit_index = 1:length(sequence)
        if sequence(bit_index) == 1
            bit_value = 1;
        else
            bit_value = -1;
        end
        
        for sample_per_bit_index = 1:L/length(sequence)
            signal(signal_sample_index) = bit_value;
            signal_sample_index = signal_sample_index + 1;
        end
    end
end