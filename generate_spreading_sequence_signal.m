function spreading_signal  = generate_spreading_sequence_signal( SS, Nb, L )

spreading_signal = zeros(1,L);
sKey = length(SS);

vAux = zeros(1,sKey*Nb);

for i=1:Nb
    for j=1:sKey
        vAux((i-1)*sKey+j) =  SS(j);    
    end
end

size_aux = L/(sKey*Nb);

for i=1 : sKey*Nb
    for j=1 : size_aux
     spreading_signal((i-1)*size_aux+j) = vAux(1,i); 
    end
end

end

