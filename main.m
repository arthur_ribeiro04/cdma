clear;
close all;

% Sequncias de espalhamento
SS1 = [1 -1 -1 1 -1 1]; %usurio 1
SS2 = [1 1 -1 -1 1 1]; %usurio 2

% Mensagens
original_sequence_1 = [1 1 0 1 1 0 1 0]; % sequncia original do usurio 1
original_sequence_2 = [0 1 1 1 1 0 1 1]; % sequncia original do usurio 2

% Constantes
Fs = 480;                       %frequncia de amostragem
Tb = 1;                         %tempo de um bit enviado
Nb = 8;                         %nmero de bits
Fc = 10;                        %frequncia da portadora
Ts = 1/Fs;                      %perodo de amostragem
t = 0:Ts:(Nb*Tb-Ts);            %vetor do tempo
L = length(t);                  %comprimento do vetor tempo
NFFT = 2^nextpow2(L);           %nmero de pontos a serem utilizados na FFT
f = Fs/2*linspace(-1,1,NFFT);   %vetor frequncia (para sinais no domnio frequncia)

% 1 & 2)
%prepara o sinal da sequencia original
original_signal_1 = generate_signal_from_binary_sequence(original_sequence_1, L);
original_signal_2 = generate_signal_from_binary_sequence(original_sequence_2, L);
%perpara sequencia espalhada da sequencia original
spread_sequence_1 = generate_spread_sequence(original_sequence_1, SS1);
spread_sequence_2 = generate_spread_sequence(original_sequence_2, SS2);
%prepara sinal da sequencia espalhada
spread_signal_1 = generate_spread_signal(spread_sequence_1, L);
spread_signal_2 = generate_spread_signal(spread_sequence_2, L);

% Prepara mensagens
sent_message_1 = strcat('Mensagem do usurio 1: ', mat2str(original_sequence_1));
sent_message_2 = strcat('Mensagem do usurio 2: ', mat2str(original_sequence_2));

%Plot dos sinais da mensagem 1 (no tempo) 
figure(1)
subplot(2,1,1)
plot(t,original_signal_1,'b')
axis([0,8,-1.20,1.20])
title(sent_message_1)
xlabel("Sinal Mensagem 1 no tempo (Original)")
subplot(2,1,2)
plot(t,spread_signal_1,'r')
axis([0,8,-1.20,1.20])
xlabel("Sinal Mensagem 1 no tempo (Espalhada)")

%Plot dos sinais da mensagem 2 (no tempo)
figure(2)
subplot(2,1,1)
plot(t,original_signal_2,'b')
axis([0,8,-1.20,1.20])
title(sent_message_2)
xlabel("Sinal Mensagem 2 no tempo (Original)")
subplot(2,1,2)
plot(t,spread_signal_2,'r')
axis([0,8,-1.20,1.20])
xlabel("Sinal Mensagem 2 no tempo (Espalhada)")

% 3) Aplica a transformada de Fourier, movendo as componentes de frequncia
% zero para o centro do array, e realiza o plot do sinal resultante no domnio 
% frequncia.

frequency_original_signal_1 = fftshift(fft(original_signal_1, NFFT));
frequency_original_signal_2 = fftshift(fft(original_signal_2, NFFT));

frequency_spread_signal_1 = fftshift(fft(spread_signal_1, NFFT));
frequency_spread_signal_2 = fftshift(fft(spread_signal_2, NFFT));

%Plot dos sinais da mensagem 1 (na frequencia) 
figure(3)
subplot(2,1,1)
plot(f,real(frequency_original_signal_1),'b')
%axis([0,8,-1.20,1.20])
title(sent_message_1)
xlabel("Sinal Mensagem 1 na frequencia (Original)")
subplot(2,1,2)
plot(f,real(frequency_spread_signal_1),'r')
%axis([0,8,-1.20,1.20])
xlabel("Sinal Mensagem 1 na frequencia (Espalhada)")

%Plot dos sinais da mensagem 2 (na frequencia) 
figure(4)
subplot(2,1,1)
plot(f,real(frequency_original_signal_2),'b')
%axis([0,8,-1.20,1.20])
title(sent_message_1)
xlabel("Sinal Mensagem 2 na frequencia (Original)")
subplot(2,1,2)
plot(f,real(frequency_spread_signal_2),'r')
%axis([0,8,-1.20,1.20])
xlabel("Sinal Mensagem 2 na frequencia (Espalhada)")


% 4) Transmite os dois sinais espalhados simultaneamente (operao de soma)
send_signal = spread_signal_1 + spread_signal_2;

% Transforma o sinal resultante para o domnio frequncia
send_signal_in_frequency = fftshift(fft(send_signal, NFFT));
filtered_signal = send_signal_in_frequency;

% Filtra o sinal no domnio frequncia
filterOffset = NFFT/Fc; % < -409.6 ou > 409.6

for frequency = 1:length(f)
    if f(frequency) < -filterOffset || f(frequency) > filterOffset
        filtered_signal(frequency) = 0;
    end
end

% Aplica a transformada inversa de Fourier para recuperar o sinal no
% domnio tempo.
received_signal = real(ifft(ifftshift(filtered_signal)));
received_signal = received_signal(1:L);

% Gera o sinais correspondentes s sequncias de espalhamento dos usurios
spreading_signal_1 = generate_spreading_sequence_signal(SS1, Nb, L);
spreading_signal_2 = generate_spreading_sequence_signal(SS2, Nb, L);

% Multiplica o sinal recebido pelo sinal gerado a partir da sequncia de
% espalhamento de cada usurio
received_signal_user_1 = received_signal .* spreading_signal_1;
received_signal_user_2 = received_signal .* spreading_signal_2;

% Recupera a sequncia enviada a partir do sinal resultante
received_sequence_user_1 = recover_sequence(received_signal_user_1, SS1, L, Nb);
received_sequence_user_2 = recover_sequence(received_signal_user_2, SS2, L, Nb);

% Plot do sinal das sequncias recuperadas no domnio tempo
received_signal_user_1 = generate_signal_from_binary_sequence(received_sequence_user_1, L);
received_signal_user_2 = generate_signal_from_binary_sequence(received_sequence_user_2, L);

figure(5)
subplot(2,1,1)
plot(t,received_signal_user_1,'b')
axis([0,8,-1.20,1.20])
title("Sinal das mensagens recuperadas no dominio tempo")
xlabel("Mensagem 1 - "+mat2str(received_sequence_user_1));
subplot(2,1,2)
plot(t,received_signal_user_2,'b')
axis([0,8,-1.20,1.20])
xlabel("Mensagem 2 - "+mat2str(received_sequence_user_2));


